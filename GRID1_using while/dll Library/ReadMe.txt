Wyatt
PrintLibrary Rev 1.0
March 2011

==============================================================
==============================================================
ABOUT

The Print Library provides TWO methods that will print two dimensional grids (9x9) 
to look like sudoku puzzles.

       8 3 1 | 9 6 2 | 5 4 7
       7 5 4 | 8 1 3 | 9 2 6
       9 6 2 | 7 5 4 | 1 8 3
       ---------------------
       5 1 7 | 4 9 8 | 6 3 2
       4 2 9 | 6 3 1 | 7 5 8
       3 8 6 | 5 2 7 | 4 1 9
       ---------------------
       1 9 5 | 2 8 6 | 3 7 4
       2 4 3 | 1 7 9 | 8 6 5
       6 7 8 | 3 4 5 | 2 9 1
                           
One method is for windows, the other is for console programs.
The method does not return a vlue.

==============================================================
==============================================================
INSTALLING

Copy the PrintLibrary.dll file to your project directory and create 
a reference to it. (right click on project in Solution Explorer / Add Reference)

===============================================================
===============================================================
CALLING

Windows
========

    GridPrint.WinPrintGridToListBox( grid, lstOut );

        grid:   a 2-dimensional int array (any name is ok)
        
        lstOut: a listbox (any name is ok)
                 make sure it is a sufficiently large listbox
                 *** WORKS BEST WITH MONO-SPACE FONTS (COURIER NEW) ***
                 
        defintion:
        public static void WinPrintGridToListBox(int[,] gridIn, System.Windows.Forms.ListBox list)
                 

Console
=======

    GridPrint.ConPrintGrid( grid );
    
        grid:   a 2-dimensional int array (any name)
        
        definition:
        public static void ConPrintGrid(int[,] gridIn)