﻿/* John VanSickle   3.14.2013   Grids Part A assignment
 * 
 * Purpose: To test knowledge of 2-D arrays by making grids
 * with certain requirements based on type of grid (Latin Square, Rows, etc.)
 * 
 * Algorithm:   -Get user input as to what grid to generate
 *              -Fill rows and cols
 *              -while filling with random nums 1-9 check generated number 
 *              against other nums in grid based on type of grid chosen
 *                  -use while loops in Latin Square for more readable code.
 *              -If a number repeats in a column or row, the current row is restarted 
 *              until a valid order is found
 *              
 *              -For Sudoku, it also..
 *                  -turns the current 3X3 grid into a 1D array
 *                  -use foreach loop to compare each number to all numbers before it
 *                  -if repeat found, large, 2d array resets column and goes back one row
 * 
 * */

using System;
using System.Collections.Generic;
using System.Diagnostics; //for timer
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRID1_using_while
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            char selection;

            //do is for repeated grid generating if desired
            do
            {
                try
                {
                    sw.Reset(); //resets for proper time reporting

                    //menu usage for user
                    Console.WriteLine("Enter:\n0. Quits \n1. Simple Grid: Random nums \n2: Simple Grid: No reps in rows\n"
                        + "3: Latin Square: No Row or Column reps\n4: Sudoku Grid: No Row, Col, or Inner-Grid Repeats\n");

                    selection = char.Parse(Console.ReadLine());

                    //uses switch statement instead of many else ifs
                    switch (selection)
                    {
                        case '0':
                            Environment.Exit(0);
                            break;

                        case '1':
                            int[,] gridArray = RandomGrid(9, 9);
                            sw.Start();
                            GridPrint.ConPrintGrid(gridArray);
                            sw.Stop();
                            Console.WriteLine("Time taken to generate grid: " + sw.Elapsed);
                            break;

                        case '2':
                            int[,] gridArray1 = RowsGrid(9, 9);
                            sw.Start();
                            GridPrint.ConPrintGrid(gridArray1);
                            sw.Stop();
                            Console.WriteLine("Time taken to generate grid: " + sw.Elapsed);
                            break;

                        case '3':
                            int[,] gridArray2 = LatinSquare(9, 9);
                            sw.Start();
                            GridPrint.ConPrintGrid(gridArray2);
                            sw.Stop();
                            Console.WriteLine("Time taken to generate grid: " + sw.Elapsed);
                            break;

                        case '4':
                            int[,] gridArray3 = Sudoku(9, 9);
                            sw.Start();
                            GridPrint.ConPrintGrid(gridArray3);
                            sw.Stop();
                            Console.WriteLine("Time taken to generate grid: " + sw.Elapsed);
                            break;

                        default:
                            Console.WriteLine("Invalid selection made, please relaunch the program.");
                            Console.ReadKey();
                            goto case '0';
                    }

                    Console.WriteLine("Press enter to continue.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
            //does not tell user about typing esc to close, maintenance only designed
            while (Console.ReadLine() != "esc");
        }

        //generates a 9X9 grid ful of random numbers from 1-9
        static int[,] RandomGrid(int rows, int cols)
        {
            int[,] array = new int[rows, cols];
            Random rand = new Random();

            //nested loop to fill in 2d array
            for (int r = 0; r < rows; r++)
                for (int c = 0; c < cols; c++)
                {
                    //inserts a random number
                    array[r, c] = rand.Next(1, 10);
                }

            return array;
        }

        //generates a 9X9 grid of random numbers with no repeats in each row
        static int[,] RowsGrid(int rows, int cols)
        {
            int[,] array = new int[rows, cols];
            Random rand = new Random();
            int i;

            //nested for loop to fill in 2d array
            for (int r = 0; r < rows; r++)
                for (int c = 0; c < cols; c++)
                {
                    array[r, c] = rand.Next(1, 10);
                    i = 0;

                    //this for tests current array spot against the other spots in same row
                    while (i < c)
                    {
                        //triggers when current num is found as a repeat
                        if (array[r,c] == array[r,i])
                        {
                            array[r, c] = rand.Next(1, 10);
                            i = -1;//sets to -1 to restart the row check for the new random num.
                        }
                        i++;
                    }
                }

            return array;
        }

        //generates a 9X9 grid of random numbers with no repeats in columns nor rows
        static int[,] LatinSquare(int rows, int cols)
        {
            int[,] array = new int[rows, cols];
            Random rand = new Random();
            int i,j;

            //nested for loop to fill in 2d array
            for (int r = 0; r < rows; r++)
                for (int c = 0; c < cols; c++)
                {
                    array[r, c] = rand.Next(1, 10);
                    i = 0;
                    j = 0;

                    //this for tests current array spot against the other spots in same row
                    while (i < c)
                    {
                        if (array[r, c] == array[r, i])
                        {
                            array[r, c] = rand.Next(1, 10); //if the current num is repeat, new random is entered.
                            i = -1; //if repeat, the row check is restarted
                        }
                        i++;
                    }

                    //this tests current array spot against other spots in same column
                    while (j < r)
                    {
                        if (array[r, c] == array[j, c])
                        {
                            
                            c = -1;//resets current column to refill row
                            break;
                        }
                        j++;
                    }
                }

            return array;
        }

        //generates a 9X9 grid of random numbers with no repeats in coumns nor rows nor inner 3X3 grids 
        static int[,] Sudoku(int rows, int cols)
        {
            int[,] array = new int[rows, cols];
            Random rand = new Random();
            int i, j;
            bool redoRow;

            //nested for loop to fill in 2d array
            for (int r = 0; r < rows; r++)
                for (int c = 0; c < cols; c++)
                {
                    redoRow = false;
                    array[r, c] = rand.Next(1, 10);
                    i = 0;  //var for checking rows for dups
                    j = 0;  //var for checking cols for dups


                    //this for tests current array spot against the other spots in same row
                    while (i < c)
                    {
                        if (array[r, c] == array[r, i])
                        {
                            array[r, c] = rand.Next(1, 10);
                            i = -1;
                        }
                        i++;
                    }

                    //this tests current array spot against other spots in same column
                    while (j < r)
                    {
                        if (array[r, c] == array[j, c])
                        {
                            //resets the column
                            c = -1;
                            //sets to true to prevent going into the continueing statements after breaking
                            redoRow = true;
                            break;
                        }
                        j++;
                    }

                    if (redoRow == true)
                        continue;

                    //tests the current 3X3 "mini" grid
                    if ((r+1) % 3 == 0 && (c+1) % 3 == 0)   //need +1 because of base 0 arrays
                    {
                        if (IsThereDuplicates(array, r, c)) //if true, resets column and decrements one row
                        {
                            c = -1;
                            r = r - 1;
                        }
                    }
                }

            return array;
        }

        //method to test if there are duplicates in a given 3X3 grid
        public static bool IsThereDuplicates(int[,] array, int r, int c)
        {
            //1D array for easier comparison
            int[] miniArray = new int[9];

            //fills a mini array with current 2D 3X3 mini grid
            for (int w = 0, lr = r - 2; lr <= r; lr++)  //w = 0 needs to be in this for loop so it does not reset
                for (int lc = c - 2; lc <= c; lc++, w++) //increments w so that each spot is filled
                {
                    miniArray[w] = array[lr, lc];
                }

            //tracks farthest num in array for comparison
            int position = 0;

            //foreach guarantees that each number in the mini array will be looked at - just trying a weird application for a foreach
            foreach (int k in miniArray)
            {
                //count is the number being tested against the current farthest position
                for (int count = 0; count < position; count++)
                {
                    if (miniArray[count] == miniArray[position])
                        return true;
                }
                position++;
            }

            return false;
        }
    }
}
